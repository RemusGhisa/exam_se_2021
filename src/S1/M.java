package S1;

public class M {

}

class A {

    private final M m;

    public A(){
        this.m = new M();
    }

    public void metA(){

    }

}

class B {

    private M paramM;

    public B(M paramM){
        this.paramM = paramM;
    }

    public void metB(){

    }
}

class Base {

    public float n;

    public void f(int g){

    }
}

class L extends Base{

    private long t;

    private M paramM;

    public void f(){

    }
}

class X {

    public void i(L l){

    }
}