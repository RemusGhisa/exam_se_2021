package S2;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class BThreads implements Runnable {

    int countStop = 2;

    private static final Object lock= new Object();
    public void run(){
        Thread t = Thread.currentThread();
        synchronized (lock) {

                if (t.getName()=="BThread3")
                    countStop++;

                for(int k=0; k<countStop; k++) {
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss");
                    LocalTime localTime = LocalTime.now();
                    System.out.println(t.getName() + "-" + dtf.format(localTime) );
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

        }



    }

    public static void main(String[] args) {

        BThreads c1 = new BThreads();
        BThreads c2 = new BThreads();
        BThreads c3 = new BThreads();

        Thread t1 = new Thread(c1,"BThread1");
        Thread t2 = new Thread(c2,"BThread2");
        Thread t3 = new Thread(c3,"BThread3");

        t1.start();
        t3.start();
        t2.start();

    }
}